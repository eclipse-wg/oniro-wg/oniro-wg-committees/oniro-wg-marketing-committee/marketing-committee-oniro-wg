# Working group meeting 2023-06-28

Meeting starts at 14:30 UTC

Meeting is quorate, but no formal agenda is prepared. No minutes come to be approved.

## Attendees

Carlo Piana - Member WG
Clark Roundy - Eclipse
Chiara Del Fabbro - Member WG
Karolina Kulig - Software Mansion
Claire Biasco - IOTex

## General status update

Chiara reports on several meetings with Need to reshape the message. The Link between Oniro and Open Harmony is going to be stronger, more visible and apparent.

We are going through a revised narrative / storytelling. Possibly before EclipseCon 2023 we will complete the task.

Claire expresses satisfaction for being included in the project.  Also Karolina.

Chiara informs that there will be hopefully a talk @ EclipseCon 2023 relevant to Oniro by Software Mansion.

Why is it relevant for people to join. Chiara will start circulating in one week or two a relevant deck. New eyes will be very helpful shaping a new message. Checking if they have sufficient rights to Gitlab to work on any documents.

## Conferences

September will be too early to present the new Eclipse Oniro at the conference Huawei is sponsoring.  

Chiara runs through the essence of Community Day at EclipseCon. We probably we will have something in the line we have done last years.

Carlo confirms he will be present independently of the acceptance of the CFP proposal, since he will be keynoting on the 18th for OSI.

## Resolutions

No resolutions.

## AOB

None:

Meeting closes at 15:00 UTC
