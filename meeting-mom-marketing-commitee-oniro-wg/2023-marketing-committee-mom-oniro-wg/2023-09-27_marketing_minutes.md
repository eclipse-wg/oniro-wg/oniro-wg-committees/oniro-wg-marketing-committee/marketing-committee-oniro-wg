# Working group meeting 2023-09-27

Meeting starts at 14:35 UTC

## Agenda
- New Chair of the marketing committe
- Eclipsecon 2023 - BoF Coordination - Participation proposals and timing
- Oniro 3.0 approach - confirm no doubts where we going and www can work on marketing message
- Web update - mention the plan to do it for 2024
- AOB

## Attendees

- Jarek - Huawei
- Carlo - Array
- Karolina - Software Mansion
- Juan - Eclipse Foundation
- Clark - Eclipse Foundation


## General status update
- Jarek (Huawei) will be acting as Chair of the Marketing Committe replacing Chiara

- Eclipsecon 2023:

1. Cross-collaboration - Two talks confirmed for IoT community Day
2. Oniro is responsible for the organization of a BoF event - link here - https://www.eclipsecon.org/2023/bofs/bof-oniro-one-os-all 

| Topic | Presenter | Status|
| ------ | ------ | -----|
| Intro to Oniro WG       | Suhail        | Confimed |
| OpenHarmony intro | OpenHarmony | Pending|
| IP compliance Toolchain | Array | Confirmed |
| React Native integration in Oniro | Software Mansion| Confirmed |
| Servo integration in Oniro | Futurwei |Pending|
| Oniro 3.0        | Huawei       | Confirmed |
 

Timekeeper - Juan

Check if we have some time extra if the room is empty for the next hour.

3. Oniro booth - Participation and shift organization
 
 Robodog showing OpenHarmony 
 
 ReactNative demo coordinated with Software Mansion

 SmartKettle - Cooperation with Smarter (in the pipeline for making them WG members)

 Robot based on Oniro 2.0 + Bosch Open Source solutions.

Servo integration - Still to be confirmed.

Indepndent thread for Organization of Eclipsecon - 


- (NOT COVERED IN THE CALL) Oniro 3.0 - Link to the recording -  https://eclipse.zoom.us/rec/play/Aco6uA5-D3dzwsGgF-yrNG27sKyNia8Q9X1CcP-DudNz40fB41UKLRzSBRK0FOpsxVfF7qLKlCTYB819.MFRnD_xDpkaXuI_d 

- (NOT COVERED IN THE CALL) Oniro website for 2024 - Team working on it

## Conferences
- Eclipsecon already covered during the call


## Resolutions

## AOB

None:

Meeting closes at 15:03 UTC
