# Working group meeting 2023-09-27

Meeting starts at 14:38 UTC

## Agenda
- Recap of Eclipsecon
- Marketing Plan 2024
- AOB

## Attendees

- Jarek - Huawei
- Suhail - Huawei
- Juan - Eclipse Foundation
- Clark - Eclipse Foundation


## General Status update

- Since it was only Huawei and EF in the call the recap of Eclipsecon was skipped. There will be a dedicated meeting to analyze the event and the results of the survey
- Program plan is in an advance state of development, so marketing plan will come after.
- Marketing plan should act as the framework and the activities we would like to develop, it will help the WG to prioritize and focus to get the most out of the resources we have
- Mkt plan 2023 (only as reference) - https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/oniro-wg-marketing-committee/marketing-committee-oniro-wg/-/blob/main/oniro-wg-marketing-plan/2023-marketing-plan-oniro-wg/2023-Oniro-WG-Marketing-Plan-draft.pdf?ref_type=heads 


## Conferences
- It will be shared a doc to contribute. We have the list from 2023 and also the ones already identified for 2024 by EF. Nevertheless it would be necessary to have all the WG members contributing to the list.



## Resolutions

## AOB

None:

Meeting closes at 15:03 UTC
