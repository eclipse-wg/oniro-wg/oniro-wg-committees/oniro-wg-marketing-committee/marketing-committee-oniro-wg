# Working group meeting 2023-11-08

Meeting starts at 14:35 UTC

## Agenda
-  Marketing plan for 2024 
- Short term plan for increasing project communication outreach
- AOB

## Attendees

- Jarek - Huawei
- Bill - Linaro
- Karolina - Software Mansion
- Juan - Eclipse Foundation
- Mats - Futurewei

## Marketing Plan 2024

- The meeting started with a recap of the current status of the WG in terms of marketing actions:
    - Web update
    - Blog post about Eclipsecon activities
    - Social media activity
- Next steps towards an effective definition of the marketing plan for 2024 covers the following aspects
    - Events - List all the events that can be relevant for the WG. Not just big ones, but also interesting opportunities to promote Oniro and its adoption in the industrial and developers communities.
    - Testimonials - It is currently one of the outdated aspects of Oniro website and the objective is to fix it. Moreover, it is proposed to do kind of short interview to WG representatives that can be also used to promote them in Social Media. 
    - Innovations - To describe the main innovations/assets we are creating in Oniro. It is important to communicate properly why the are relevant and what value they bring.
    - Competitors benchmark - To highlught our key strengths it is necessary to know what other solutions in the same domain offers. It is critical to have contributions from all WG members to perform this analysis.
- The next weeks the WG will work on developing a short term social media publications calendar. The objective is to validate our capability to keep this area active enough to open new communication channels (Linkedin, Mastodon).

## Conferences
- Once the list is ready during the meetings will be evaluated the opportunities for participating in each of them and with which level of commitment.
 

## Resolutions

## AOB

None:

Meeting closes at 15:08 UTC
