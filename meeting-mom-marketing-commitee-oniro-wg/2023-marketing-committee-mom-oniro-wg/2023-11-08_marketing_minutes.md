# Working group meeting 2023-11-08

Meeting starts at 14:35 UTC

## Agenda
- Marketing Plan 2024
- AOB

## Attendees

- Jarek - Huawei
- Suhail - Huawei
- Juan - Eclipse Foundation
- Clark - Eclipse Foundation
- Mats - Futurewei

## Marketing Plan 2024

- The meeting started with a recap of the strategic marketing objectives set in the approved Oniro Program Plan for 2024.
- To achieve those objectives and given the number of resources for marketing activities in the WG, we will be working based on an organised framework that can help us to identify the key strengths, our main selling points and translate them into coherent messages that will help us to communicate Oniro status, value and objectives better.
- The framework starts in a spreadsheet that will be shared by email with the WG to fill several aspects:

1.  Oniro assets - a consistent list of the assets Oniro already has and some additional information to identify their status and the audiences that can benefit from that.
2. Developers community - the Oniro architecture is clearly divided into several layers, it is important to identify the level of contributions, relevance and innovation the WG will be contributing in 2024. The idea is to detect those who are more relevant to help the WG to prioritize them.
3. Vertical Industries - the objective is to identify which industries will benefit the most from Oniro solutions so we can define stories that can resonate with them. Since Oniro as an OS platform can target almost any industry, the objective will be to narrow the list and then identify the prospects to whom Oniro solutions will be presented.
4. Events - this is the list of events with additional information where Oniro as a whole or specific projects and contributions can fit.
5. Social Media communication - one of the challenges for 2024 is to have the ability to communicate all the work the WG is developing and do it towards building awareness. In an effort to educate about Oniro to the community, it is going to be defined as an agreed calendar of external communication actions.
6. Oniro prospects - this will be the list of candidates entities to be contacted by the Working Group to share how Oniro can help them first, and the benefits of being part of the WG then.
7. Oniro Cheatsheet - This is a collection of the key messages, contacts and resources the WG can always use to support conversations with the different audiences.

- The proposal will be open for contributions for the next 2 weeks until the next marketing committee and it is expected that contributions from all WG members. Once the first round of contributions is there, all the content will be moved into Gitlab to support openness and accessibility to all the actions.
- It was noticed that the plan should include a time frame to support the ideas and actions - it was agreed to include them in the marketing plan and follow these milestones quarterly.
- It was also commented that since Oniro is now taking an additional approach with the contribution of OpenHarmony, the WG should also work on clearly identifying which content applies to which platform.

## Conferences
- Once the list is ready during the meetings will be evaluated the opportunities for participating in each of them and with which level of commitment.



## Resolutions

## AOB

None:

Meeting closes at 15:08 UTC
