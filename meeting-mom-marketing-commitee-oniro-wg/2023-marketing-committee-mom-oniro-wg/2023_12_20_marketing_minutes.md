# Working group meeting 2023-12-20

Meeting starts at 14:35 UTC

## Agenda
- Marketing plan for 2024 
- Social media communication plan
- AOB

## Attendees

- Jarek - Huawei
- Mats - Futurewei
- Juan - Eclipse Foundation
- Clark - Eclipse Foundation

## Marketing Plan 2024

- Marketing plan can be found here - https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/oniro-wg-marketing-committee/marketing-committee-oniro-wg/-/blob/main/oniro-wg-marketing-plan/2024-marketing-plan-oniro-wg/Oniro_Marketing_Plan_2024_v1.0.pdf?ref_type=heads
- The four pillars for 2024 are:
    - Story line - to clarify the key messages Oniro wants to share with the community
    - Brand Awareness - to promote Oniro as an entity and the value in the OS field
    - Collaborations - within the EF nd related projects and also with other OS projects available fitting into Oniro Scope
    - Cooperation with Open Harmony - coordinate actions to promote both solutions Oniro and OpenHarmony
- It is also noted that the key for our success in our communication strategy is content creation, this is a responsibility of the WG and to simplify this part it is worthy to develop the story-telling.
- To follow the actions it has been created a space in gitlab to track the actions that need to be implemented, starting with a strong focus on story telling - https://gitlab.eclipse.org/eclipse-wg/oniro-wg/marketing-oniro/oniro-storyline-development/-/boards

## Social Media Planning
- Oniro is targeting to show as a vibrant community and Social Media can help to achieve this objective.
- Oniro has set a Social Media calendar to help the WG to coordinate the activities and simplify the organization. The calendar will be follow here - https://gitlab.eclipse.org/eclipse-wg/oniro-wg/marketing-oniro/social-media-contributions-calendar/-/boards

## Conferences
- Conferences opportunities will be followed through -https://gitlab.eclipse.org/eclipse-wg/oniro-wg/marketing-oniro/events-oniro/-/boards
 
## Resolutions
- Marketing Chair and EF to consolidate the calendar for social media contributions and the testimonials from the WG members to be distributed during the first week of January

## AOB
- It was asked about the publication of the agreement with OpenAtom foundation - Mike Milinkovich blog post will be released in January 
None:

Meeting closes at 15:10 UTC
