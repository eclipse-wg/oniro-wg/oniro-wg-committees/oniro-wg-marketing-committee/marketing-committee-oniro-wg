# Working group meeting 2023-08-23

Meeting starts at 14:30 UTC

Meeting is quorate, but no formal agenda is prepared. No minutes come to be approved.

## Attendees

- Carlo - Array
- Claire - IoTex
- Karolina - Software Mansion
- Clark - Eclipse Foundation
- Juan - Eclipse Foundation


## General status update
- Chiara from Huawei left and we need to select a new Marketing Committee Chair.
- Array shared that after a first conversation with Huawei it is still not fully clear how the changes in the technical approach will impact the ongoing projects. IoTex confirm that they don't know how this will impact the work developed. 
- The definition of the marketing messages will be done once the changes are understood by WG members to better represent Oniro for the coming months.

## Conferences
- It was confirmed our community day will be happening during the Eclipsecon on Monday Oct. 15th during the afternoon.
- It is expected to cross-collaborate with other WGs (IoT and Edge or SDV)
- Formal communication and the call for topics will be arrive in the next days.

## Resolutions
- Organize a open session to present to the WG how the changes coming in Oniro is going to impact the WG and projects. This should happen before the finalization of the Program Plan stting first half of September as target.

## AOB

None:

Meeting closes at 14:55 UTC
