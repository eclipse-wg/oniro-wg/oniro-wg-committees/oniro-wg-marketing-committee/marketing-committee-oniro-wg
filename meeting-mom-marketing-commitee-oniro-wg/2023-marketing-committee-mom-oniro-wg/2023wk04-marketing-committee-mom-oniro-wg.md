# Oniro Working Group Marketing Committee Meeting 2023wk02

Minutes of Meeting state: pending approval by the Oniro Marketing Committee

[comment]: # (Approved on YYYY-MM-DD commit xxxx)

* Date: 2023-01-25 2023wk04
* Time: from 15:00 to 16:00 UTC 
   * Oniro WG Marketing Committee Meeting: 15:00 to 15:30 UTC
   * Oniro WG Marketing Committee All Marketing Representatives: 15:30 to 16:00 UTC
* Chair: Chiara DelFabbro
* Minutes taker: Carlo Piana
* Join via Zoom
	* #link <https://eclipse.zoom.us/j/87562802390?pwd=OFFHVUJmSC9FakFwOVBIY3ZqVk5uZz09>
	* Meeting ID: 875 6280 2390
	* Passcode: 682767

## Agenda

Oniro WG Marketing Committee Meeting
* Approval of the Minutes of Meeting from 2023-01-11 2023wk02 - Chiara 5 minutes
* Oniro WG Marketing PLan 2023 draft - Chiara 15 min
* Resolutions - 5 min
* AoB - 5 min

Oniro WG Marketing Committee All Marketing Representatives

* Oniro WG Marketing PLan 2023 draft - Chiara 20 min
* FOSDEM: coordination - Agustin 05 min
* AoB (Open Floor) - 5 min

## Attendees

### Oniro WG Marketing Committee Meeting

Oniro WG Marketing Committee Members (Quorum =  1 of 2 for this Meeting. Ballots and resolutions require 2 of 2):

* Carlo Piana - Array
* Chiara Del Fabbro - Huawei
* Adrian O'Sullivan - Eclipse

[comment]: # (* Alberto Pianon - Array alternate)

Marketing Committee Regrets:


Other Attendees:


Eclipse Foundation: 

* Agustin Benito Bethencourt
* Clark Roundy


### Oniro WG Marketing Committee All Marketing Representatives

Additional attendees from the Oniro WG Marketing Committee meeting

* Luca Tavanti - Cubit
* Steeve - IoTeX

## Minutes 

### Oniro WG Marketing Committee Meeting

#### Approval of the Minutes of Meeting from 2023-01-11 2023wk02
 
Link to the minutes #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/oniro-wg-marketing-committee/marketing-committee-oniro-wg/-/blob/main/meeting-mom-marketing-commitee-oniro-wg/2023-marketing-committee-mom-oniro-wg/2023wk02-marketing-committee-mom-oniro-wg.md>

Approved commit 8fce3b55

#### Oniro WG Marketing Plan 2023 draft

Oniro WG Marketing Plan 2023 draft in...
* .pptx format #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/oniro-wg-marketing-committee/marketing-committee-oniro-wg/-/blob/main/oniro-wg-marketing-plan/2023-marketing-plan-oniro-wg/2023-Oniro-WG-Marketing-Plan-draft.pptx>
* .odp format #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/oniro-wg-marketing-committee/marketing-committee-oniro-wg/-/blob/main/oniro-wg-marketing-plan/2023-marketing-plan-oniro-wg/2023-Oniro-WG-Marketing-Plan-draft.odp>
* .pdf format #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/oniro-wg-marketing-committee/marketing-committee-oniro-wg/-/blob/main/oniro-wg-marketing-plan/2023-marketing-plan-oniro-wg/2023-Oniro-WG-Marketing-Plan-draft.pdf>

Members like the plan, only Clark express concern that we might have too little resources and would need to reset priorities. Also, focus on events to attend. 

Chiara will collect more feedback and provide another iteration at next meeting.

#### Resolutions

To approve the Minutes of 2023-01-11 2023wk02

#### AoB  

None


### Oniro WG Marketing Committee All Marketing Representatives

Opens at 15:35 UTC

#### Oniro WG Marketing PLan 2023 draft
 
* Check the links above
* Went over the comments on the document
* Agustin will add the 2023 program plan objectives to the document.
* Agustin will send a call for feedback through the public marketing mailing list.

#### FOSDEM

Outstanding tasks:
* IoTeX announcement including social media campaign #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/marketing-oniro/events-oniro/-/issues/29>
* Social media campaign:
   * Announcement of our talks at the event and IoTeX announcement. #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/marketing-oniro/events-oniro/-/issues/31>
   * Video promotion #link <>

IoTex will finish their announcement (something is outdated). Preferably on or before Wednesday

Agustin will provide a ticket to use in social media so that he can track. Done

Relevant links:

* Event wiki page #link <https://gitlab.eclipse.org/groups/eclipse-wg/oniro-wg/marketing-oniro/-/wikis/Events/FOSDEM'23>
   * Participants list: #link <https://gitlab.eclipse.org/groups/eclipse-wg/oniro-wg/marketing-oniro/-/wikis/Events/2023-events/FOSDEM'23#participants-list>
   * Oniro related talks: #link <https://gitlab.eclipse.org/groups/eclipse-wg/oniro-wg/marketing-oniro/-/wikis/Events/2023-events/FOSDEM'23#talks>
* Website #link <https://fosdem.org/2023/>
* Stands #link <https://fosdem.org/2023/stands/>

#### AoB

None

## Links and references

* Repository where you will find the agenda and meeting minutes #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/oniro-wg-marketing-committee/marketing-committee-oniro-wg/-/tree/main/meeting-mom-marketing-commitee-oniro-wg>
* Oniro WG marketing mailing list (open) #link <https://accounts.eclipse.org/mailing-list/oniro-wg-marketing>
* Oniro WG Marketing Committee mailing list #link <https://accounts.eclipse.org/mailing-list/oniro-wg-marketing-committee>
* Marketing content and events repository: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/marketing-oniro>
* Oniro wiki #link <https://gitlab.eclipse.org/groups/eclipse-wg/oniro-wg/marketing-oniro/-/wikis/home>

* AoB: Any Other Business
* MoM: Minutes of Meeting
* OA: OpenAtom (Foundation)

Meeting closes at 16:04 UTC
