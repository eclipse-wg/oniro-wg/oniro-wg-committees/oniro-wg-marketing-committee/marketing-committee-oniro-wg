# Oniro Working Group Marketing Committee Meeting 2023wk02

Minutes of Meeting state: pending approval by the Oniro Marketing Committee

Approved on 2023-01-25 commit 8fce3b55

* Date: 2023-01-11 2023wk02
* Time: from 15:00 to 16:00 UTC 
   * Oniro WG Marketing Committee Meeting: 15:00 to 15:30 UTC
   * Oniro WG Marketing Committee All Marketing Representatives: 15:30 to 16:00 UTC
* Chair: Chiara DelFabbro
* Minutes taker: Carlo Piana
* Join via Zoom
	* #link <https://eclipse.zoom.us/j/87562802390?pwd=OFFHVUJmSC9FakFwOVBIY3ZqVk5uZz09>
	* Meeting ID: 875 6280 2390
	* Passcode: 682767

## Agenda

Oniro WG Marketing Committee Meeting
* Approval of the Minutes of Meeting from 2022-10-05 2022wk40 - Chiara 5 minutes
* IoTEX membership official communication - Chiara 5 min
* FOSDEM - Chiara 10 min
* Resolutions - 5 min
* AoB - 5 min

Oniro WG Marketing Committee All Marketing Representatives

* MWC - Chiara 5 min
* Embedded World - Chiara 5 min
* AoB (Open Floor) - 5 min

## Attendees

### Oniro WG Marketing Committee Meeting

Oniro WG Marketing Committee Members (Quorum =  1 of 2 for this Meeting. Ballots and resolutions require 2 of 2):


* Carlo Piana - Array
[comment]: # (* Alberto Pianon - Array alternate)
* Chiara Del Fabbro - Huawei

Marketing Committee Regrets:


Other Attendees:


Eclipse Foundation: 

* Agustin Benito Bethencourt
[comment]: # (* Clark Roundy)

### Oniro WG Marketing Committee All Marketing Representatives


Additional attendees from the Oniro WG Marketing Committee meeting

* Luca Tavanti - Cubit

## Minutes 

### Oniro WG Marketing Committee Meeting

#### Approval of the Minutes of Meeting from 2022-10-05 2022wk40
 
Link to the minutes #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/oniro-wg-marketing-committee/marketing-committee-oniro-wg/-/blob/main/meeting-mom-marketing-commitee-oniro-wg/2022-marketing-committee-mom-oniro-wg/2022wk40-marketing-committee-mom-oniro-wg.md> 

Since there was no quurum, the minute of that meeting is not approved.

#### IoTEX membership official communication

Ticket describing the activity #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/marketing-oniro/events-oniro/-/issues/29>

Chiara will look into the ticket and fill in gaps.


#### FOSDEM

Relevant links:
* Event wiki page #link <https://gitlab.eclipse.org/groups/eclipse-wg/oniro-wg/marketing-oniro/-/wikis/Events/FOSDEM'23>
* Website #link <https://fosdem.org/2023/>
* Stands #link <https://fosdem.org/2023/stands/>
* Schedule #link <https://fosdem.org/2023/schedule/>
   * Embedded, Mobile and Automotive devroom #link <https://fosdem.org/2023/schedule/track/embedded_mobile_and_automotive/>
   * Microkernel and Component-based OS devroom #link <https://fosdem.org/2023/schedule/track/microkernel_and_component_based_os/>
   * Security devroom #link <https://fosdem.org/2023/schedule/track/security/>
   * Image-based Linux and Secure Measured Boot devroom #link <https://fosdem.org/2023/schedule/track/image_based_linux_and_secure_measured_boot/>
   * Open Source Firmware, BMC and Bootloader devroom #link <https://fosdem.org/2023/schedule/track/open_source_firmware_bmc_and_bootloader/>
   * Open Research Tools and Technology devroom #link <https://fosdem.org/2023/schedule/track/open_research_tools_and_technology/>

EF has a booth at level 1 - Building K, behind the theatre room, with FSFE, Apache and other Free Software entities, good opportunity, we can explore if we can display something (we don't know if Ettore is going to come).

We need to draft a list of attendants and try and coordinate. 

#### Resolutions

No resolution was moved to.


#### AoB  

* Huawei needs to communicate the Marketing representative alternate.


### Oniro WG Marketing Committee All Marketing Representatives

Opens at 15:30 UTC


#### MWC

The idea is to co-host a workshop in the venue or close to it. 10-15 people. Talk different projects. Open Services Cloud.


#### Embedded World

EF is going to have a booth. It's the first time and it's very expensive. We need to discuss with other people involved how this can pan out.


#### AoB


## Links and references

* Repository where you will find the agenda and meeting minutes #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/oniro-wg-marketing-committee/marketing-committee-oniro-wg/-/tree/main/meeting-mom-marketing-commitee-oniro-wg>
* Oniro WG marketing mailing list (open) #link <https://accounts.eclipse.org/mailing-list/oniro-wg-marketing>
* Oniro WG Marketing Committee mailing list #link <https://accounts.eclipse.org/mailing-list/oniro-wg-marketing-committee>
* Marketing content and events repository: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/marketing-oniro>
* Oniro wiki #link <https://gitlab.eclipse.org/groups/eclipse-wg/oniro-wg/marketing-oniro/-/wikis/home>

* AoB: Any Other Business
* MoM: Minutes of Meeting
* OA: OpenAtom (Foundation)

Meeting closes at 15:43 UTC
