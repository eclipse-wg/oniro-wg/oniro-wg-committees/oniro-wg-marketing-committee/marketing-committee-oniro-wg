# Oniro Working Group Marketing Committee Meeting 2022wk40

Minutes of Meeting state: no quorum so no formal meeting

[comment]: # (Approved on YYYY-MM-DD commit xxxx)

* Date: 2022-10-05 2022wk40
* Time: from 14:00 to 15:00 UTC 
   * Oniro WG Marketing Committee Meeting: 14:00 to 14:30 UTC
   * Oniro WG Marketing Committee All Marketing Representatives: 14:30 to 15:00 UTC
* Chair: Chiara DelFabbro
* Minutes taker: Carlo Piana
* Join via Zoom
	* #link <https://eclipse.zoom.us/j/87562802390?pwd=OFFHVUJmSC9FakFwOVBIY3ZqVk5uZz09>
	* Meeting ID: 875 6280 2390
	* Passcode: 682767

## Agenda

Oniro WG Marketing Committee Meeting
* Approval of the Minutes of Meeting from 2022-08-24 2022wk34 - Chiara 5 minutes
* 2023 planning and budget – Chiara 15 min
* Resolutions - 5 min
* AoB - 5 min

Oniro WG Marketing Committee All Marketing Representatives
* Welcome Politecnico Milano - Antonio and Francesco 10 minutes
* EclipseCon updates and work in progress – Agustin/Chiara 10 min
* 2022 release roadmap – Chiara 5 min
* AoB (Open Floor) - 5 min

## Attendees

### Oniro WG Marketing Committee Meeting

Oniro WG Marketing Committee Members (Quorum =  1 of 2 for this Meeting. Ballots and resolutions require 2 of 2):


[comment ]: # (* Carlo Piana - Array)
[comment ]: # (* Donghi He - Huawei alternate)
[comment ]: # (* Alberto Pianon - Array)
[comment ]: # (* Chiara Del Fabbro - Huawei)

Marketing Committee Regrets:



Other Attendees:


Eclipse Foundation: 

[comment ]: # (* Agustin Benito Bethencourt)
[comment ]: # (* Clark Roundy)


## Minutes 

### Oniro WG Marketing Committee Meeting

#### Approval of the Minutes of Meeting from 2022-08-24 2022wk34
 
Link to the minutes #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/marketing-oniro/marketing-committee-oniro-wg/-/blob/main/meeting-mom-marketing-commitee-oniro-wg/marketing-committee-mom-oniro-wg-2022wk34.md> 

Commit xxxxx

#### 2023 planning and budget

Link to the current draft:
* Link to the current working version: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/tree/main/program-plan-and-budget-oniro-wg/program-plan-budget-2023/program_plan_2023_drafts>


#### Resolutions


#### AoB  

None


### Oniro WG Marketing Committee All Marketing Representatives

Opens at HH:MM UTC

#### Attendees 

Additional attendees from the Oniro WG Marketing Committee meeting
* Aurore Perchet (Huawei)
* Luca Tavanti (Cubit)


#### Welcome Politecnico Milano 



#### EclipseCon updates and work in progress

* Link to the ELCE 2022 wiki page: #link <https://gitlab.eclipse.org/groups/eclipse-wg/oniro-wg/marketing-oniro/-/wikis/Events_CFP_participation/Open-Source-Summit>


#### 2022 release roadmap 



#### AoB


## Links and references

* Repository where you will find the agenda and meeting minutes #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/oniro-wg-marketing-committee/marketing-committee-oniro-wg/-/tree/main/meeting-mom-marketing-commitee-oniro-wg>
* Oniro WG marketing mailing list (open) #link <https://accounts.eclipse.org/mailing-list/oniro-wg-marketing>
* Oniro WG Marketing Committee mailing list #link <https://accounts.eclipse.org/mailing-list/oniro-wg-marketing-committee>
* Marketing content and events repository: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/marketing-oniro>
* Oniro wiki #link <https://gitlab.eclipse.org/groups/eclipse-wg/oniro-wg/marketing-oniro/-/wikis/home>

* AoB: Any Other Business
* MoM: Minutes of Meeting
* OA: OpenAtom (Foundation)

Meeting closes at 16:44 UTC
