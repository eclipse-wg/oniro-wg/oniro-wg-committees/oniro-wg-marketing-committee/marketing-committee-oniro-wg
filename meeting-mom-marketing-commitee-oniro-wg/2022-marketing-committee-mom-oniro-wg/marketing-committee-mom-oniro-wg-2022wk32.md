# Oniro Working Group Marketing Committee Meeting 2022wk32

Minutes of Meeting state: pending approval by the Oniro Marketing Committee

approved on 2022-08-24 commit ff6bff14

* Date: 2022-08-10 2022wk32
* Time: from 14:00 to 15:00 UTC 
   * Oniro WG Marketing Committee Meeting: 14:00 to 14:30 UTC
   * Oniro WG Marketing Committee All Marketing Representatives: 14:30 to 15:00 UTC
* Chair: Chiara DelFabbro
* Minutes taker: Carlo Piana
* Join via Zoom
	* #link <https://eclipse.zoom.us/j/87562802390?pwd=OFFHVUJmSC9FakFwOVBIY3ZqVk5uZz09>
	* Meeting ID: 875 6280 2390
	* Passcode: 682767

## Agenda

Oniro WG Marketing Committee Meeting
* Approval of the Minutes of Meeting from 2022-06-29 2022wk26 - Chiara 5 minutes
* Oniro WG Marketing Plan description - Chiara/Clark 15 min
* Resolutions - 5 min
* AoB - 5 min

Oniro WG Marketing Committee All Marketing Representatives
* Oniro WG Marketing Plan description - Chiara/Clark 25 min
* AoB (Open Floor) - 5 min

## Attendees

### Oniro WG Marketing Committee Meeting

Oniro WG Marketing Committee Members (Quorum =  1 of 2 for this Meeting. Ballots and resolutions require 2 of 2):

* Chiara Del Fabbro - Huawei
* Carlo Piana - Array

Marketing Committee Regrets:


[comment]: # (Other Attendees:)
[comment]: # (* Donghi He - Huawei alternate)

Eclipse Foundation: 

* Agustin Benito Bethencour
* Clark Roundy


## Minutes 

### Oniro WG Marketing Committee Meeting

#### Approval of the Minutes of Meeting from 2022-06-29 2022wk26
 
Link to the minutes #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/marketing-oniro/marketing-committee-oniro-wg/-/blob/main/meeting-mom-marketing-commitee-oniro-wg/marketing-committee-mom-oniro-wg-2022wk26.md> 

Commit a53530e3

#### Oniro WG Marketing Plan description

Link to the current draft:
* Link to the .pdf version: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/oniro-wg-marketing-committee/marketing-committee-oniro-wg/-/blob/main/oniro-wg-marketing-plan/2022-Oniro-Marketing-Plan-draft.pdf>
* Link to the .pptx version: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/oniro-wg-marketing-committee/marketing-committee-oniro-wg/-/blob/main/oniro-wg-marketing-plan/2022-Oniro-Marketing-Plan-draft.pptx>

The program plan is going to be sent over. In two weeks time we expect comments and possibly approval. 

- The Group discusses in the light of future events and the relevance of marketing plan with reference to the Program Plan. 
- Wee need to make the Mission statement consistent with the one in the Project Charter.


#### Resolutions

Approve minutes of meeting from Week 26


#### AoB  




### Oniro WG Marketing Committee All Marketing Representatives

Opens at 14:30 UTC

#### Attendees

In addtion to the above...


* Aurore Perchet (Huawei)
* Luca Tavanti (Cubit)

Discussion on Marketing Plan continues

- Luca has questions about how the the MP can make an impact and wonders if -- once the defined audience is defined -- the planned actions are directed to this audience. 
- Comments from everybody is welcome


#### AoB

None


## Links and references

* Repository where you will find the agenda and meeting minutes #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/oniro-wg-marketing-committee/marketing-committee-oniro-wg/-/tree/main/meeting-mom-marketing-commitee-oniro-wg>
* Oniro WG marketing mailing list (open) #link <https://accounts.eclipse.org/mailing-list/oniro-wg-marketing>
* Oniro WG Marketing Committee mailing list #link <https://accounts.eclipse.org/mailing-list/oniro-wg-marketing-committee>
* Marketing content and events repository: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/marketing-oniro>
* Oniro wiki #link <https://gitlab.eclipse.org/groups/eclipse-wg/oniro-wg/marketing-oniro/-/wikis/home>

* AoB: Any Other Business
* MoM: Minutes of Meeting
* OA: OpenAtom (Foundation)

Meeting closes at 15:00 UTC sharp
