# Oniro Working Group Marketing Committee Meeting 2022wk26

Minutes of Meeting state: pending approval by the Oniro Marketing Committee

approved on 2022-08-10 commit a53530e3

* Date: 2022-06-29 2022wk26
* Time: from 14:00 to 15:00 UTC 
   * Oniro WG Marketing Committee Meeting: 14:00 to 14:30 UTC
   * Oniro WG Marketing Committee All Marketing Representatives: 14:30 to 15:00 UTC
* Chair: Chiara DelFabbro
* Minutes taker: Carlo Piana
* Join via Zoom
	* #link <https://eclipse.zoom.us/j/87562802390?pwd=OFFHVUJmSC9FakFwOVBIY3ZqVk5uZz09>
	* Meeting ID: 875 6280 2390
	* Passcode: 682767

## Agenda

Oniro WG Marketing Committee Meeting
* Approval of the Minutes of Meeting from 2022-06-01 2022wk22 - Chiara 5 minutes
* Linkedin account creation and management - Chiara 5 min
* Follow up of the OpenAtom - Eclipse communication around Oniro-OpenHarmony collaboration - Chiara 5 min
* Follow up on the Oniro website - Chiara/Clark 5 min
* Resolutions - 5 min
* AoB - 5 min

Oniro WG Marketing Committee All Marketing Representatives
* New Oniro member: Presentation of Cubit Innovation Lab - Luca T. 15 minutes
* Follow up Oniro Marketing Program Plan - Chiara/Clark 5 min
* IoTWeek 2022 reprt - Chiara 5 minutes
* EW2022 reprt - Alessasndra/Agustin - 5 min
* AoB (Open Floor) - 5 min

## Attendees

### Oniro WG Marketing Committee Meeting

Oniro WG Marketing Committee Members (Quorum =  1 of 2 for this Meeting. Ballots and resolutions require 2 of 2):

* Chiara Del Fabbro - Huawei
* Carlo Piana - Array

Marketing Committee Regrets:


[comment]: # (Other Attendees:
[comment]: # (* * Aurore Perchet- Huawei alternate)

Eclipse Foundation: 
* Agustin Benito Bethencourt
* Yves Maître
[comment]: # (* Clark Roundy) 


## Minutes 

### Oniro WG Marketing Committee Meeting

#### Approval of the Minutes of Meeting from 2022-06-01 2022wk22
 
Link to the minutes #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/marketing-oniro/marketing-committee-oniro-wg/-/blob/main/meeting-mom-marketing-commitee-oniro-wg/marketing-committee-mom-oniro-wg-2022wk22> 

026455b3

#### Linkedin account creation and management

Vote to create and manage a Linkedin account. @oniroproject 

Chiara: We need more content otherwise we end up advertising third party events and content. We should wait until then (eg EclipseCon we'll have enough content) and concentrating on promoting Oniro per se. It's not needed.

Carlo: We should tell people not to be affiliate just yet.

Yves: would be a total disaster to have and not making it content-rich.

Carlo: we need to reserve the name for avoiding squatting.

Agusting: What - Can we vote when we have a concrete proposal?

Also, naming convention for single projects is discussed. We need to collect more information as to what's being done by other OS projects inside EF. Agustin shall open a ticket. 


#### Follow up of the OpenAtom - Eclipse communication around Oniro-OpenHarmony collaboration

Chiara shares wiki pages. Some talks for the OpenAtom conferences have been submitted, we have no idea which is going to be picked up, probably Davide's, but what bout the others? A lot is changing day to day.

#### Follow up on the Oniro website

Chiara reports on the changes and what is being projected content-wise. Also, how much we stress Oniro-OpenAtom narrative?

#### Resolutions

- Minutes Approved 026455b3

- Not to use OpenAtom TM on marketing except in specific circumstances


#### AoB  

Chiara: Need to bring up the Community day for EclipseCon for Oniro.

Chiara: Gadgets. Do we have to put the two logos (OA-Oniro) in gadgets? Agustin reports it's not in the Agreement. Carlo suggests using the logo needs a license on TM and it's not advisable to do it anyway on gadgets -- only in commonly suggested marketing activity. Agustin adds even to states compatibility the specs project needs to deliver.

Consensus is reached that we should drop any ideas to use OA TM on marketing except in specific circumstances

### Oniro WG Marketing Committee All Marketing Representatives

Opens at 14:42

#### Attendees

Gianluca Tavanti (Cubit)
Doghni He (Huawei)
Salvatore Cina (Huawei)

#### New Oniro member: Presentation of Cubit Innovation Lab

Luca T. presents the company and takes questions from the attendees. Link to the presentation #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/marketing-oniro/visuals-resources-oniro/-/blob/main/deck-visuals-resources-oniro/cubit/CUBIT_pres_2022.07_ENG.pdf>

#### Follow up Oniro Marketing Program Plan

Chiara reports on current (not finalized) status of it. Almost ready for collecting comments

#### IoTWeek 2022 report 

We are short on time. Chiara reports. The target is more education-university-R&D and most of the people met are researchers and scholars. The overall organization was not brilliant and the value of the meeting not very high either. Logistics difficult. Someone asked how to join, the contacts will be passed as potential lead.


#### EW2022 - Alessandra/Agustin - 5 min

Short on time and Alessandra/Ettore cannot attend. So, postpone.


## Links and references

* Repository where you will find the agenda and meeting minutes #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/oniro-wg-marketing-committee/marketing-committee-oniro-wg/-/tree/main/meeting-mom-marketing-commitee-oniro-wg>
* Oniro WG marketing mailing list (open) #link <https://accounts.eclipse.org/mailing-list/oniro-wg-marketing>
* Oniro WG Marketing Committee mailing list #link <https://accounts.eclipse.org/mailing-list/oniro-wg-marketing-committee>
* Marketing content and events repository: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/marketing-oniro>
* Oniro wiki #link <https://gitlab.eclipse.org/groups/eclipse-wg/oniro-wg/marketing-oniro/-/wikis/home>

* AoB: Any Other Business
* MoM: Minutes of Meeting
* OA: OpenAtom (Foundation)

Meeting closes at 15:04 UTC
