# Oniro Working Group Marketing Committee Launch Meeting 2022wk14

Minutes of Meeting state: approved by the Oniro Marketing Committee

* Date: 2022-04-06
* Time: from 15:00 to 16:00 UTC 
   * Oniro WG Marketing Committee Meeting: 15:00 to 15:30 UTC
   * Oniro WG Marketing Committee All Marketing Representatives: 15:30 to 16:00 UTC
* Chair: Chiara DelFabbro
* Minutes taker: Carlo Piana
* Join via Zoom
	* #link <https://eclipse.zoom.us/j/87562802390?pwd=OFFHVUJmSC9FakFwOVBIY3ZqVk5uZz09>
	* Meeting ID: 875 6280 2390
	* Passcode: 682767

## Agenda

Oniro WG Marketing Committee Meeting
* Approval of the Minutes of Meeting from 2022-03-23 - Chiara 5 minutes
* IP transfer to EF - Chiara 10 minutes
* Resolutions - 5 min
* AoB - 10 min

Oniro WG Marketing Committee All Marketing Representatives
* Marketing Plan: initial draft - Chiara 15 minutes --> https://docs.google.com/presentation/d/1dqAukBUl7eU5XxAPlYmc1YsFf-FWKhH10EcZI5oFBRc/edit?usp=sharing
* Social media accounts - Shanda & Chiara 10 min
* AoB (Open Floor) - 5 min

## Attendees

### Oniro WG Marketing Committee Meeting

Oniro WG Marketing Committee Members (Quorum =  1 of 2 for this Meeting. Ballots and resolutions require 2 of 2):
* Chiara Del Fabbro - Huawei
* Carlo Piana - Array

Marketing Committee Regrets:

Other Attendees:


Eclipse Foundation: 
* Agustin Benito Bethencourt

Oniro WG Marketing Committee All Marketing Representatives

The previous ones and the following additions
* Dongni He (Huawei)

## Minutes 

### Oniro WG Marketing Committee Meeting

#### Approval of the Minutes of Meeting from 2022-03-23

#link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/marketing-oniro/marketing-committee-oniro-wg/-/blob/main/meeting-mom-marketing-commitee-oniro-wg/marketing-committee-mom-oniro-wg-2022wk12>

Minutes are approved.

#### IP transfer to EF 

The issue pertains to the transfer of IP to EF or to give it full rights to use the artifacts and components therein included and permit so by all members of WG. This requirement would be satisfied by releasing the content in an admissible Creative Commons license, and CC-BY should be sufficient. Subject to confirmation. Also subject to confirmation it would be sufficient that there is a confirmation by the member of the WG that has contracted the agency that the agreement includes a consistent clause. No need to share the relevant agreement.

Technical details of the work we expect from the agency on the website (using Hugo, preferably), will be discussed separately. No resolution is tabled. 

#### Resolutions

* Approval of minutes

* Switch timing to 14:00 UTC. Changes will be communicated throught new invites

* Ratify Chiara and Dongni as additional social media admins

* Keep Mastodon with a pinned message that it is inactive (decided in the All Marketing Representatives part)

#### AoB  

Discussing the timing of meetings, with DST in Northern Hemispfere it could be more convenient to shift from 15:00 UTC to 14:00 UTC so keep the same spot as initially for most of the people. It is so decided with unanimous vote.


### Oniro WG Marketing Committee All Marketing Representatives

#### Marketing Plan: initial draft.

Chiara has worked on it and shared an initial draft to elicit feedback. We will also present it to the next marketing SC for the open session. After these interaction and comments from EF, we will table formal approval. 

#### Social media accounts

It is being asked if there are objections to ratify Chiara and Dongni He to be added as admins for Onrio all social media accounts, in addition to the marketing team of Eclipse. None recorded. Unanimously approved.

Discussing opening Linkedin account for Oniro. We will explore the technicalities to make sure we are in a position to manage it. We will discuss if it's sustainable.  Same for tools to manage the different accounts to have an efficient workflow to feed all the social media. No decisions is therefore taken.

Mastodon: decide whether to keep it, keep it silent, shut it down. We need to find a way to cross feed from Twitter, or to appoint some dedicated person of sufficiently high profile to create the content (Dony). For the time being consensus is to keep it open but with a message saying that the account is momentarily inactive with an invite to follow us elsewhere (Twitter). It is so unanimously decided.

#### AoB  

None raised. Meeting is closed at 16:08 UTC.

## Links and references

* Repository where you will find the agenda and meeting minutes #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/marketing-oniro/maketing-committee-oniro-wg/-/tree/main/meeting-mom-marketing-commitee-oniro-wg>

* AoB: Any Other Business
* MoM: Minutes of Meeting
