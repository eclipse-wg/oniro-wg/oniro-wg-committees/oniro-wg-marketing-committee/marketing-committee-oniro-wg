# Oniro Working Group Marketing Committee Meeting 2022wk34

Minutes of Meeting state: pending approval by the Oniro Marketing Committee

Approved on 2022-08-24 commit ff6bff14

* Date: 2022-08-24 2022wk34
* Time: from 14:00 to 15:00 UTC 
   * Oniro WG Marketing Committee Meeting: 14:00 to 14:30 UTC
   * Oniro WG Marketing Committee All Marketing Representatives: 14:30 to 15:00 UTC
* Chair: Chiara DelFabbro
* Minutes taker: Carlo Piana
* Join via Zoom
	* #link <https://eclipse.zoom.us/j/87562802390?pwd=OFFHVUJmSC9FakFwOVBIY3ZqVk5uZz09>
	* Meeting ID: 875 6280 2390
	* Passcode: 682767

## Agenda

Oniro WG Marketing Committee Meeting
* Approval of the Minutes of Meeting from 2022-08-10 2022wk32 - Dony 5 minutes
* Oniro website - Clark 15 min
* Resolutions - 5 min
* AoB - 5 min

Oniro WG Marketing Committee All Marketing Representatives
* Oniro WG Marketing Plan description - Chiara/Clark 10 min
* Oniro at EclipseCon 2022 - Agustin 5 minutes
* Oniro at OSS EU/ELCE - Dony 5 minutes
* Oniro at OSXP - Agustin 5 minutes
* AoB (Open Floor) - 5 min

## Attendees

### Oniro WG Marketing Committee Meeting

Oniro WG Marketing Committee Members (Quorum =  1 of 2 for this Meeting. Ballots and resolutions require 2 of 2):


* Carlo Piana - Array
* Donghi He - Huawei alternate
* Alberto Pianon - Array

Marketing Committee Regrets:

* Chiara Del Fabbro - Huawei

Other Attendees:


Eclipse Foundation: 

* Agustin Benito Bethencourt
* Clark Roundy until 14.32


## Minutes 

### Oniro WG Marketing Committee Meeting

#### Approval of the Minutes of Meeting from 2022-08-10 2022wk32
 
Link to the minutes #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/marketing-oniro/marketing-committee-oniro-wg/-/blob/main/meeting-mom-marketing-commitee-oniro-wg/marketing-committee-mom-oniro-wg-2022wk32.md> 

Commit ff6bff14

#### Oniro WG Marketing Plan description

Link to the current draft:
* Link to the current working version: #link <https://docs.google.com/presentation/d/1UPgP4a_tnuWSoqEXIKIHyiQqDqn7bIKGO49XLwcqiPc/edit#slide=id.g13b9e707bb8_0_142>

* You should have access to this document. If that is not the case, please contact Agustin B.B..
* This document is only for editing purposes during this process. When this process is done, a .pptx and .pdf version will be published at Oniro WG Gitlab and we will use them to collaborate.

There are some comments from Luca Travanti which should be better explained, since Clark has not been able to see what precisely is the point (see local comment), so it's better that we wait for Luca to be in the call or ping him offline. Then we put the PDF online and this will be the basis of the approval next time.

#### Oniro website

Agustin describes the situation, we are awaiting for a mockup to see the layout and then we sould understand what content would go in there.

Apparently the cost is very hefty and we should as other to make a pitch for the job, we agree to look around for alternatives. Dony agrees to share the RFQ and requirements to have alternatives.


#### Resolutions

- minutes for WK 32 are approved


#### AoB  

None


### Oniro WG Marketing Committee All Marketing Representatives

Opens at 14:31 UTC

#### Attendees

Same as private 

[comment]: # (* Aurore Perchet (Huawei))
[comment]: # (* Luca Tavanti (Cubit))


[comment]: # (#### Oniro WG Marketing Plan description)
[comment]: # ()
[comment]: # (Link to the current draft:)
[comment]: # (* Link to the current working version: #link <https://docs.google.com/presentation/d/1UPgP4a_tnuWSoqEXIKIHyiQqDqn7bIKGO49XLwcqiPc/edit#slide=id.)g13b9e707bb8_0_142>
[comment]: # ()
[comment]: # (If you do not rights to access this document, please tell Agustin.)

#### Oniro at EclipseCon 2022

* Link to the EclipseCon 2022 wiki page #link <https://gitlab.eclipse.org/groups/eclipse-wg/oniro-wg/marketing-oniro/-/wikis/Events/EclipseCon>

Discussion about participation and events, such as Hackerspace (probably we should organize regular meeting with organizers because it's the first time we are attempting one), talks (depending also on the presence of which developer, who could be required to spend more time on the project itself).

#### Oniro at OSS EU/ELCE

* Link to the ELCE 2022 wiki page: #link <https://gitlab.eclipse.org/groups/eclipse-wg/oniro-wg/marketing-oniro/-/wikis/Events_CFP_participation/Open-Source-Summit>

There we will have opportunity to meet potential members and there will be space for demonstration. Eclipse has a booth, Huawei is sponsoring. Giveaways for the booth? Dony: yes, pending details as to how to ship, numbers of items to deliver.


#### Oniro at OSXP 

* Link to the OSXP 2022 wiki page: #link <https://gitlab.eclipse.org/groups/eclipse-wg/oniro-wg/marketing-oniro/-/wikis/Home/Events_CFP_participation/OSXP>

#### AoB

None




## Links and references

* Repository where you will find the agenda and meeting minutes #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/oniro-wg-marketing-committee/marketing-committee-oniro-wg/-/tree/main/meeting-mom-marketing-commitee-oniro-wg>
* Oniro WG marketing mailing list (open) #link <https://accounts.eclipse.org/mailing-list/oniro-wg-marketing>
* Oniro WG Marketing Committee mailing list #link <https://accounts.eclipse.org/mailing-list/oniro-wg-marketing-committee>
* Marketing content and events repository: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/marketing-oniro>
* Oniro wiki #link <https://gitlab.eclipse.org/groups/eclipse-wg/oniro-wg/marketing-oniro/-/wikis/home>

* AoB: Any Other Business
* MoM: Minutes of Meeting
* OA: OpenAtom (Foundation)

Meeting closes at 16:44 UTC
