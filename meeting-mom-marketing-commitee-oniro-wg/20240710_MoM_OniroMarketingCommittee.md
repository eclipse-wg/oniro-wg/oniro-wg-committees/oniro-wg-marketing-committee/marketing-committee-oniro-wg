# Working group marketing meeting 2024-7-10

Meeting starts at 14:35 UTC

## Agenda

- Resource page of the website update
- OCX
- Events participation 
- AOB

## Attendees

- Jarek - Huawei
- Juan - Eclipse Foundation
- Amin - Eclipse Foundation
- Mats - Futurewei


## Resource page of the website update
- We agree on taking advantage of all the events in which the project is participating
- The place for providing those materials is resource page of the website
- there are other EF WG that can be taken as reference as Sparkplug
- It is necessary to engage the whole WG for:
    - Report events in which Oniro is participating
    - Collect the materials presented
    - Get outcomes from the participation that are ready to share - i.e. video recordings, reports
- To track it we will use the board with a few updates
    - New columns to record those events in which we participate and got the materials.

## OCX
- Support Huawei in their needs for the booth during OCX for all aspects related to Oniro
- Support the talks accepted and that are going to be presented by Oniro members
- Potential side events aligned with Oniro - TBC

## Events participation
- The space is open in Gitlab to add and suggest actions on the existing ones.
- link to the tracking board - https://gitlab.eclipse.org/eclipse-wg/oniro-wg/marketing-oniro/events-oniro/-/boards
 

## Resolutions

## AOB

None:

Meeting closes at 15:03 UTC

