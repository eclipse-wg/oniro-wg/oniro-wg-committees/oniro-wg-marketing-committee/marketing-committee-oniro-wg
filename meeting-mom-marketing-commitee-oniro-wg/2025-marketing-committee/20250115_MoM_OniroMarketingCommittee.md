# Working group meeting 2025-1-15

Meeting starts at 15:35 UTC

## Agenda

- Content plan for 2025
- AOB

## Attendees

- Mats Lundgren - Futurewei
- Juan - Eclipse Foundation
- Amin - Eclipse Foundation

## Content Plan for 2025
- A google doc will be shared to start working on the questions for the video interviews. 
- A series of posts will be prepared to promotoe WG FOSDEM activities


## Events participation
- The space is open in Gitlab to add and suggest actions on the existing ones.
- link to the tracking board - https://gitlab.eclipse.org/eclipse-wg/oniro-wg/marketing-oniro/events-oniro/-/boards
 

## Actions

## Resolutions

## AOB

None:

Meeting closes at 16:03 UTC
