# Working group meeting 2024-3-13

Meeting starts at 14:35 UTC

## Agenda

- New communications - social media - How to promote our channels
- Assets - short term needs - 
- Owners for the communication architecture and FAQs
- AOB

## Attendees

- Jarek - Huawei
- Juan - Eclipse Foundation


## New Communication Channels
- REF: https://gitlab.eclipse.org/eclipse-wg/oniro-wg/marketing-oniro/social-media-contributions-calendar/-/boards
- https://docs.google.com/document/d/1D1yU19OLnomgNsmXMORajM5zm3o9YXt2fez40U9Q5MU/edit#heading=h.p4ohj9cxl2rp-
- It has been started the communication using the LinkedIn page. Initially we are creating content to have a base, once it is done we will start with recruitment campaings.
- It would be good to have all members supporting the WG communications.

## Oniro marketing Assets
- The development of Volla phone is requiring content. A draft document is created that will be later on populating the message architecture one.

## Owners of the sections for the message architecture and FAQs
- A first proposal will be done by the Mkt Committee and the request contributions to all the WG members

## Events participation
- The space is open in Gitlab to add and suggest actions on the existing ones.
- link to the tracking board - https://gitlab.eclipse.org/eclipse-wg/oniro-wg/marketing-oniro/events-oniro/-/boards
 

## Resolutions

## AOB

None:

Meeting closes at 15:03 UTC
