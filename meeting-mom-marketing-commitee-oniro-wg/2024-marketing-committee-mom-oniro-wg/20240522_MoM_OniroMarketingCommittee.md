# Working group meeting 2024-5-22

Meeting starts at 14:35 UTC

## Agenda

- Communication Architecture document
- Volla Community days - Consolidation of the message
- Communication Actions for Oniro - OpenHarmony workshop
- IoT Survey
- Assets - short term needs -
- Events participation 
- AOB

## Attendees

- Jarek - Huawei
- Juan - Eclipse Foundation
- Amin - Eclipse Foundation


## Communication architecture document
- Development and consolidation of the messages
    - Tell me 
    - Key words
    - Key messages
- Value drafting
- We convered the strategy to make Oniro key message shining

## Volla Community days
- Further develop story telling

## Oniro - OpenHarmony workshop organization during HDC
- Consolidate messaging to support the promotion


## Events participation
- The space is open in Gitlab to add and suggest actions on the existing ones.
- link to the tracking board - https://gitlab.eclipse.org/eclipse-wg/oniro-wg/marketing-oniro/events-oniro/-/boards
 

## Resolutions

## AOB

None:

Meeting closes at 15:03 UTC
