# Working group meeting 2024-2-14

Meeting starts at 14:35 UTC

## Agenda

- Gitlab spaces with duties for social media communication
- Communication message architecture document
- Events review
- Logo Update in the Oniro website
- AOB

## Attendees

- Jarek - Huawei
- Juan - Eclipse Foundation
- Clark - Eclipse Foundation
- Karolina - SW Mansion

## Social Media Publication Calendar
-It has been created a space in gitlab with issues assigned to the responsibles for creating social media content each week.
- The content is not expected to be long and heavy, but highlights of the status of the different projects and members' contributions.
- the space can be found - https://gitlab.eclipse.org/eclipse-wg/oniro-wg/marketing-oniro/social-media-contributions-calendar/-/boards

## Oniro message architecture
- It has been prepared a document for simplify the Working
- It is based on the foundations of other WG successfully delivering their messages
- It contains
    - 1 liner
    - The WG in less than 100 words
    - Key words
    - Key messages 
        - Promise
        - Value
        - Benefits
        - Proof

- It is necessary to complete the roadmap to deliver consistent messaging but worked in parallel.

## Logos in project website
- All logos displayed in the website requires the signing of the Working Group Participation Agreement. This is the only way EF can confirm that the use of a member company logo is done foollowing the Participation agreement and acknowledge by a company representative.

## Events participation
- The space is open in Gitlab to add and suggest actions on the existing ones.
- link to the tracking board - https://gitlab.eclipse.org/eclipse-wg/oniro-wg/marketing-oniro/events-oniro/-/boards
 

## Resolutions

## AOB

None:

Meeting closes at 15:03 UTC
