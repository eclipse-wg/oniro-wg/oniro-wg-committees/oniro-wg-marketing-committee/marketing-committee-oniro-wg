# Working group meeting 2024-3-5

Meeting starts at 14:35 UTC

## Agenda

- Social media communication
- Communication message architecture document
- AOB

## Attendees

- Jarek - Huawei
- Juan - Eclipse Foundation


## Social Media Communication
- REF: https://gitlab.eclipse.org/eclipse-wg/oniro-wg/marketing-oniro/social-media-contributions-calendar/-/boards
- We went through the first social media publications we will make - https://docs.google.com/document/d/1D1yU19OLnomgNsmXMORajM5zm3o9YXt2fez40U9Q5MU/edit#heading=h.p4ohj9cxl2rp- As soon as the Linkedin channel is available, we will start the communication

## Oniro message architecture
- we go through its details - https://docs.google.com/document/d/1N_Syn4-mQ4SHRFhEvDdOr_dOqZZBDCK8w4H4M74sd5M/edit#heading=h.s3nm0p82oypk - As soon as the roadmap is consolidated we will proceed with it, I will share it in the SC meeting to get contributions from as many WG members as possible.

## Logos in project website
- Typefox logo already included as they officially joined the WG 

## Events participation
- The space is open in Gitlab to add and suggest actions on the existing ones.
- link to the tracking board - https://gitlab.eclipse.org/eclipse-wg/oniro-wg/marketing-oniro/events-oniro/-/boards
 

## Resolutions

## AOB

None:

Meeting closes at 15:03 UTC
