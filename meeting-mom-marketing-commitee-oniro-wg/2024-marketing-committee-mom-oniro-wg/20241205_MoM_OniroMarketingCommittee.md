# Working group meeting 2024-12-5

Meeting starts at 15:35 UTC

## Agenda

- Planning for 2025 
- AOB

## Attendees

- Francesco Pham - Huawei
- Scarlet - Huawei
_ Jarek - Huawei (Excused, updated offline)
- Mats Lundgren - Futurewei
- Juan - Eclipse Foundation
- Amin - Eclipse Foundation


## Plan for 2025
- PLan for 2025 considers 4 areas of cooperation:
    - Social media communications:
        - How to keep the communication pace fostering not just informing, but a Call to Action that helps us to get more adopters and eventually members
    - Content:
        - How to produce more content well aligned with the WG activities that can drive asoption.
    - Events:
        - Combine participation and sponsoring 
        - organize of a Hackathon
    - Cooperation with OpenHarmony:
        - Events

### Social media communication
- Keep promoting the channels Linkedin, X and Mastodon
- Build the content strategy around:
    - Key technical milestones
    - Events we are participating/sponsoring

### Content creation Strategy
- Exploit the possibilities of video interviews:
    - Make a 5/6 questions interview for IoT and mobile
    - Extract video pills
- Cases study to be explored

### Events
- Promote our participation at events and consolidate info to share.
- Organize one hackathon (IoT)
- Work closely with Universities - as it is planned but limiting WG resource put on it. 

### Cooperation with OpenHarmony
- Continue the cooperation and try to organize workshop as done during 2024

## Events participation
- The space is open in Gitlab to add and suggest actions on the existing ones.
- link to the tracking board - https://gitlab.eclipse.org/eclipse-wg/oniro-wg/marketing-oniro/events-oniro/-/boards
 

## Actions
    - EF to prepare a draft light marketing plan to be shared before next Marketing Committee
## Resolutions

## AOB

None:

Meeting closes at 16:03 UTC
