# Working group meeting 2024-1-17

Meeting starts at 14:35 UTC

## Agenda

- Events participation in 2024
- Questionaire for WG members - interview
- Social media plan for H1 2024
- Web site update management
- AOB

## Attendees

- Jarek - Huawei
- Juan - Eclipse Foundation
- Mats - Futurewei

## Events participation 2024
- There is a board in Gitlab to track the events available and the proposals submitted by the WG. - https://gitlab.eclipse.org/eclipse-wg/oniro-wg/marketing-oniro/events-oniro/-/boards
    - we have already a list of events that includes some organized by WG members. It would be very possitive to understand the opportunities for Oniro in those events
    - All the proposals sent, either to present Oniro as a whole or parts of the contributions to the projects would be good to be tracked.
- It was discussed the virtual events, already included in the Oniro Program and Marketing Plan to keep growing the awareness of Oniro - we will be targeting first half on June
    - The WG needs to define the content and the speakers and the EF will be supporting with the tool and promotion
    
- There is a first veriosn of the questionaire for all WG members - the idea is to use the responses to build content for social media, blog post ...

- The plan for social media posting keeps as planned.
    - The plan starts as a framework to have our communication covered
    - We have flexibility to deliver the most suitable content in each moment

- Web update - we have a process defined to have more flexibility for website editing but we need:
    - to identify the responsible for website editing - he/she will go through a training to use the platform
    - Content generation - Eclipse foundation can support, but the content and the context should be done by the WG.


## Conferences
- Commmented before
- link to the tracking board - https://gitlab.eclipse.org/eclipse-wg/oniro-wg/marketing-oniro/events-oniro/-/boards
 

## Resolutions

## AOB

None:

Meeting closes at 15:10 UTC
